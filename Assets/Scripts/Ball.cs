﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    public float m_MaxSpeed = 2;
    public GameManager m_GameManager;

    private Rigidbody2D m_RigidBody;
    private Vector3 m_StartPosition;

    // Use this for initialization
    void Start() {
        m_RigidBody = GetComponent<Rigidbody2D>();  // Get reference to the rigidbody component
        m_StartPosition = transform.position;       // Store starting position
        SetVelocity(new Vector2(0, -1));            // Set initial velocity
    }

    // Update is called once per frame
    void Update() {

    }

    // Collision logic
    void OnCollisionEnter2D(Collision2D collision) {
        // Store collision object location
        Vector3 collisionVector = collision.gameObject.transform.position;
        Bounds collisionBounds = collision.collider.bounds;
        // Check if hitting the paddle
        if (collision.gameObject.name == "Paddle") {
            PaddleBounce(collision);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Block")) {
            BlockBounce(collision);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Bounds")) {
            WallBounce(collision);
        }
        else if (collision.gameObject.name == "Ceiling") {
            CeilingBounce();
        }
        else if (collision.gameObject.name == "Floor") {
            FloorHit();
        }
    }

    void SetVelocity(Vector2 velocity) {
        velocity.Normalize();       //Normalize velocity vector
        m_RigidBody.velocity = velocity * m_MaxSpeed;    // Set rigidbody velocity
    }

    // Handle bounce logic for the paddle
    void PaddleBounce(Collision2D collision) {
        // Get the angle for bounce
        float xVelocity = (transform.position.x - collision.transform.position.x) / collision.collider.bounds.size.x;
        SetVelocity(new Vector2(xVelocity, 1));
    }

    // Handle bounce logic for blocks
    void BlockBounce(Collision2D collision) {
        // Check if the ball should bounce up or down
        Vector2 collisionVector = collision.gameObject.transform.position;
        float yVelocity = 0;
        if (transform.position.y < collisionVector.y) {
            yVelocity = -1.0f;
        }
        else {
            yVelocity = 1.0f;
        }
        // Check if the ball should bounce left or right
        float xVelocity = 0;
        if (transform.position.x < collisionVector.x) {
            xVelocity = -1.0f;
        }
        else {
            xVelocity = 1.0f;
        }
        ScorePoints(collision.gameObject);
        SetVelocity(new Vector2(xVelocity, yVelocity));
        Destroy(collision.gameObject);  // Destroy the block
    }

    // Handle bouncing off walls
    void WallBounce(Collision2D collision) {
        // X and Y velocity
        float xVector = 0;
        float yVector = m_RigidBody.velocity.y;
        // Store vector position
        Vector3 location = collision.gameObject.transform.position;
        // Manage x velocity
        // Manage y velocity
        if (location.x < transform.position.x)
            xVector = 1;
        else
            xVector = -1;
        // Set velocity
        SetVelocity(new Vector2(xVector, yVector));
    }

    // Handle bouncing off the ceiling
    void CeilingBounce() {
        float xVelocity = m_RigidBody.velocity.x;
        float yVelocity = -1.0f;
        SetVelocity(new Vector2(xVelocity, yVelocity));
    }

    // Handle ball hitting the floor
    void FloorHit() {
        transform.position = m_StartPosition;
        SetVelocity(new Vector2(0,0));
        StartCoroutine(RoundDelay());
    }

    // Delay for round reset
    IEnumerator RoundDelay() {
        yield return new WaitForSeconds(2.0f);
        int direction = Random.Range(-1, 1);
        SetVelocity(new Vector2(direction, -1));
    }

    void ScorePoints(GameObject gameObject) {
        int points = gameObject.GetComponent<Block>().points;
        m_GameManager.AddPoints(points);
    }
}

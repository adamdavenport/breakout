﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public Text m_LifeText;         // Display the player's remaining lives
    public Text m_ScoreText;        // Display the player's current score
    public Text m_MessageText;      // Display messages to the player
    public int m_StartingLives = 5;     // Number of lives the player starts with

    private int m_Score;            // Current score
    private int m_Lives;            // Remaining lives

    // Use this for initialization
    void Start() {
        m_Lives = m_StartingLives;
        m_MessageText.enabled = false;
        m_Score = 0;
        UpdatePoints();
    }

    // Called when the player loses a round
    public void RoundLoss() {
        m_Lives = m_Lives--;
        UpdateLives();
        if (m_Lives <= 0) {
            Loser();
        }
    }

    // Called when the ball hits the ground
    void Loser() {
        m_MessageText.enabled = true;       // Enable the text box
        m_MessageText.text = "Game Over";   // Set Message text to game over
    }

    // Update life total
    void UpdateLives() {
        m_LifeText.text = "Lives: " + m_Lives.ToString();       // Update text box text
    }

    // Update Score display
    void UpdatePoints() {
        m_ScoreText.text = m_Score.ToString();
    }

    // Add points
    public void AddPoints(int points) {
        m_Score += points;
        UpdatePoints();
    }
}

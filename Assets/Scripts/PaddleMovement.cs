﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleMovement : MonoBehaviour {

    public float m_MaxSpeed = 4;

    private Rigidbody2D m_RigidBody;
    private string m_InputAxis = "Horizontal";
    private float m_Velocity;

	// Use this for initialization
	void Start () {
        m_RigidBody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        Movement();
	}

    // Manage paddle movement
    void Movement() {
        m_Velocity = Input.GetAxis(m_InputAxis) * m_MaxSpeed;    // Get movement axis
        m_RigidBody.velocity = new Vector2(m_Velocity, 0);  // Set velocity
    }
}
